---
layout: page
title: About
permalink: /about/
---

Hello!

I'm Matthew, or Matt, or whatever-you-like, a (formerly) Year 13 student from
Wellington, NZ. This website is basically a record of what I get up to while
on my student exchange to England, where I will be for the next two
months.

*I'll add to this page as things happen - Check back later.*

**Website Info:** If you think the website looks fancy (I like trains, can you tell?), you can
use it for your own projects by downloading the files [here](https://gitlab.com/firox263/exchange-web).
It's licensed under CC-BY-SA so make sure you give credit.
