---
layout: post
title:  "Pre-departure Thoughts"
date:   2019-11-27 23:04:23 +1300
categories: exchange
---
Hello all and welcome to what I guess is my blog.

**tl;dr - I'm doing a thing in England for two months, and it will maybe-probably-hopefully be fun.**

I'm Matthew, or Matt, or whatever-you-like, a (formerly) Year 13 student from
Wellington, NZ. This is basically a record of what I get up to while on my student
exchange to Weymouth, England, where I will be for the next two months. I'm
not the person to normally keep a blog, so I suppose we'll see how this goes.

#### Why an Exchange?
So why am I doing an exchange you might ask? My first thought of doing
a program like this was almost exactly one year ago, to date. While on holiday in the
South Island, and bored out of my mind from <s>looking at rocks</s> sightseeing,
I thought about how I really wanted to visit England. Having been born in England
before moving to NZ when I was very young, I've thought about going there to study
or maybe even live one day *quite often* (possibly an understatement...). For some
reason, I never quite considered a student exchange up until that point, but then it clicked
and I was instantly sold on the idea.

My parents were also keen on the idea of me doing an exchange, and understandably less
keen on the idea of paying for it. Since then, I got a job, sort of surprising myself
in the process, and did more extra-curriculars than is humanly possible
(or recommended, as my sleep-deprived self might add). With exams finished, I am now
writing this two days before I leave on my exchange.

#### Thoughts
I suppose in a way I am quite nervous about the whole thing. This certainly isn't my
first time flying internationally, but at the same time, it is my first flight anywhere
*by myself*. More-so is the fact that I'll be in somebody else's house for two months. On
the other hand, **what's the worst that could happen...** (he said).

Jokes aside, I am very-much looking forward to England. I can't say I'm particularly
keen on travelling for *two days straight* to get there, but at the same time I guess
it's a 'bit of an adventure', and I'm sure it will be a lot of fun. Before I finish off,
I'd like to give a special thank you to [NZIIU](https://instagram.com/nziiustudentexchange)
for organising the whole thing.

Stay tuned for hopefully entertaining (and shorter) posts.

&mdash; Matt

<br><br>
*To keep up with my posts, you can subscribe to my [RSS Feed](/feed.xml).*
